using Microsoft.AspNetCore.Mvc;


public class ClienteController : Controller
{
static List<Cliente> lista = new List<Cliente>();

    // public string Index()
    // {
    //     return "Método Index!";
    // }

//input type password para esconder a senha

public ActionResult Index(){

    return View(lista);

}

[HttpGet] //usamos aqui para acessarmos a view, mandando para o servidor
public ActionResult Create(){

    return View();

}

[HttpPost] //usamos quando precisamos acionar alguma coisa, para receber

public ActionResult Create(Cliente model){

        lista.Add(model);

        return RedirectToAction("Index");
}

//Detalhes 
public ActionResult Details (int id){

        foreach(Cliente p in lista) {
            if(p.ClienteId == id){
                return View(p);
            }
        }

        return NotFound();
    }

//Delete

public ActionResult Delete(int id){

        foreach(Cliente p in lista){
            if (p.ClienteId == id){
                lista.Remove(p);
                break;
            }
        }

        return RedirectToAction("Index");
    }

     [HttpGet]
    public ActionResult Update(int id){

        foreach(Cliente p in lista) {
            if(p.ClienteId == id){
                return View(p);
            }
        }

        return NotFound();
    }

     [HttpPost]

    public ActionResult Update(int id, Cliente model)
    {
        foreach (Cliente p in lista)
        {
            if (p.ClienteId == id)
                // /Views/Cliente/Update.cshtml
                lista.Remove(p);
            lista.Insert(id, model);
            return RedirectToAction("Index");
        }
        return NotFound();
    }

    // public ActionResult Search(FormCollection form){

    //     return View("Index");
    // }

}
